<?php

namespace Drupal\opensearchtab\Services;

use Drupal\Core\Config\ConfigFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Generates XML code for Open Search Description.
 *
 * @package Drupal\opensearchtab
 */
class OpenSearchDescriptionXmlService {

  /**
   * System configurations.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $systemConfig;

  /**
   * Open Search Tab configurations.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $ostConfig;

  /**
   * A request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The current request.
   */
  public function __construct(ConfigFactory $config_factory, RequestStack $request_stack) {
    $this->systemConfig = $config_factory->get('system.site');
    $this->ostConfig = $config_factory->get('opensearchtab.settings');
    $this->request = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('request_stack')
    );
  }

  /**
   * Generate the XML code from configs and the current request.
   */
  public function getXml() {
    // Prepare variables for the XML content.
    $short_name = !empty($this->ostConfig->get('shortname')) ?
      $this->ostConfig->get('shortname') :
      (!empty($this->systemConfig->get('name')) ?
        $this->systemConfig->get('name') : 'Drupal site');

    $description = !empty($this->ostConfig->get('description')) ?
      $this->ostConfig->get('description') :
      (!empty($this->systemConfig->get('slogan')) ?
        $this->systemConfig->get('slogan') : 'Search the site');

    $search_form_path = !empty($this->ostConfig->get('search-form-path')) ?
      $this->request->getSchemeAndHttpHost() .
      $this->ostConfig->get('search-form-path') :
      $this->request->getSchemeAndHttpHost() . '/search/node';

    $search_path = !empty($this->ostConfig->get('search-path')) ?
      $this->request->getSchemeAndHttpHost() .
      $this->ostConfig->get('search-path') :
      $this->request->getSchemeAndHttpHost() .
      '/search/node?keys={searchTerms}';

    $suggestions_path_json = !empty($this->ostConfig->get('suggestions-path-json')) ?
      $this->request->getSchemeAndHttpHost() .
      $this->ostConfig->get('suggestions-path-json') : '';

    $suggestions_path_xml = !empty($this->ostConfig->get('suggestions-path-xml')) ?
      $this->request->getSchemeAndHttpHost() .
      $this->ostConfig->get('suggestions-path-xml') : '';

    $favicon_path = !empty($this->ostConfig->get('favicon-path')) ?
      $this->request->getSchemeAndHttpHost() .
      $this->ostConfig->get('favicon-path') : '';

    // Define the XML content.
    $openTag = new \SimpleXMLElement(
      '<OpenSearchDescription></OpenSearchDescription>'
    );
    $openTag->addAttribute(
      'xmlns', 'http://a9.com/-/spec/opensearch/1.1/'
    );
    $openTag->addAttribute(
      'xmlns:xmlns:moz',
      'http://www.mozilla.org/2006/browser/search/'
    );
    $openTag->addChild('ShortName', $short_name);
    $openTag->addChild('Description', $description);
    $searchUrl = $openTag->addChild('Url');
    $searchUrl->addAttribute('type', 'text/html');
    $searchUrl->addAttribute('method', 'get');
    $searchUrl->addAttribute('template', $search_path);
    if ($suggestions_path_json !== '') {
      $suggestionsUrl = $openTag->addChild('Url');
      $suggestionsUrl->addAttribute(
        'type', 'application/x-suggestions+json'
      );
      $suggestionsUrl->addAttribute('method', 'get');
      $suggestionsUrl->addAttribute('template', $suggestions_path_json);
    }
    if ($suggestions_path_xml !== '') {
      $suggestionsUrl = $openTag->addChild('Url');
      $suggestionsUrl->addAttribute(
        'type', 'application/x-suggestions+xml'
      );
      $suggestionsUrl->addAttribute('method', 'get');
      $suggestionsUrl->addAttribute('template', $suggestions_path_xml);
    }
    if ($favicon_path !== '') {
      $faviconUrl = $openTag->addChild('Image', $favicon_path);
      $faviconUrl->addAttribute('height', '16');
      $faviconUrl->addAttribute('width', '16');
      $faviconUrl->addAttribute('type', 'image/x-icon');
    }
    $openTag->addChild('xmlns:moz:SearchForm', $search_form_path);

    return $openTag->asXML();
  }

}
