<?php

namespace Drupal\opensearchtab\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Configures Open Search Tab settings for this site.
 */
class OpenSearchSettingsForm extends ConfigFormBase {

  /**
   * Define all text fields in the form.
   *
   * @return array
   *   Text field's parameters.
   */
  private function fields() {
    return [
      [
        'title' => $this->t('Search form path'),
        'description' => $this->t('Path to the search form, e.g.: "/search". If empty, the following value will be used: "/search/node".'),
        'name' => 'opensearchtab_search_form_path',
        'id' => 'search-form-path',
        'min_length' => 2,
      ],
      [
        'title' => $this->t('Search path'),
        'description' => $this->t('Path to the search, e.g.: "/search?{searchTerms}". If empty, the following value will be used: "/search/node?keys={searchTerms}".'),
        'name' => 'opensearchtab_search_path',
        'id' => 'search-path',
        'min_length' => 15,
      ],
      [
        'title' => $this->t('Suggestions path (JSON)'),
        'description' => $this->t('Path to the suggestions, e.g.: "/suggestions?{searchTerms}".'),
        'name' => 'opensearchtab_suggestions_path_json',
        'id' => 'suggestions-path-json',
        'min_length' => 15,
      ],
      [
        'title' => $this->t('Suggestions path (XML)'),
        'description' => $this->t('Path to the suggestions, e.g.: "/suggestions?{searchTerms}".'),
        'name' => 'opensearchtab_suggestions_path_xml',
        'id' => 'suggestions-path-xml',
        'min_length' => 15,
      ],
      [
        'title' => $this->t('Short name'),
        'description' => $this->t('Short name will be displayed in the search box. If empty, the site name be used.'),
        'name' => 'opensearchtab_shortname',
        'id' => 'shortname',
        'min_length' => 3,
      ],
      [
        'title' => $this->t('Description'),
        'description' => $this->t('Description of the search.'),
        'name' => 'opensearchtab_description',
        'id' => 'description',
        'min_length' => 10,
      ],
      [
        'title' => $this->t('Favicon path'),
        'description' => $this->t('Path to the Favicon, e.g.: "/themes/contrib/mytheme/favicon.ico".'),
        'name' => 'opensearchtab_favicon_path',
        'id' => 'favicon-path',
        'min_length' => 5,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'opensearchtab_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('opensearchtab.settings');

    $opensearchdescription_link = Link::fromTextAndUrl(
      $this->t('Open Search Description'),
      Url::fromRoute('opensearchtab.opensearchdescription')
    )->toString();

    $info_text = $this->t('See the XML of the open search tab: @link', [
      '@link' => $opensearchdescription_link,
    ]);

    $form['info_box'] = [
      '#type' => 'details',
      '#title' => $this->t('Infobox'),
      '#description' => $info_text,
      '#open' => TRUE,
    ];

    foreach ($this->fields() as $field) {
      $form[$field['name']] = [
        '#type' => 'textfield',
        '#title' => $field['title'],
        '#default_value' => $config->get($field['id']),
        '#description' => $field['description'],
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Test the fields for the minimum length.
    foreach ($this->fields() as $field) {
      if (!$form_state->isValueEmpty($field['name'])) {
        if (strlen($form_state->getValue($field['name'])) < $field['min_length']) {
          $form_state->setErrorByName($field['name'],
            $this->t(
              '@title is less than @min_length characters.',
              [
                '@title' => $field['title'],
                '@min_length' => $field['min_length'],
              ]
            )
          );
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    foreach ($this->fields() as $field) {
      $this->config('opensearchtab.settings')
        ->set($field['id'], $form_state->getValue($field['name']));
    }
    $this->config('opensearchtab.settings')->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['opensearchtab.settings'];
  }

}
