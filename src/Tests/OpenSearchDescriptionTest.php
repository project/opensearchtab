<?php

namespace Drupal\opensearchtab\Tests;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the settings form.
 *
 * @group search
 * @group opensearchtab
 */
class OpenSearchDescriptionTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['opensearchtab'];

  /**
   * Default theme.
   *
   * See: https://www.drupal.org/node/3083055
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * A request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * A simple user with 'access content' permission.
   *
   * @var \Drupal\user\Entity\User
   */
  private $user;

  /**
   * Perform any initial set up tasks that run before every test method.
   */
  public function setUp(): void {
    parent::setUp();
    $this->user = $this->drupalCreateUser(['access content']);
    $this->config = \Drupal::configFactory()
      ->getEditable('opensearchtab.settings');
    $this->config
      ->set('search-form-path', '/my-search')
      ->set('search-path', '/my-search?{searchTerms}')
      ->set('suggestions-path-json', '/suggestions?{searchTerms}')
      ->set('suggestions-path-xml', '/suggestions?{searchTerms}')
      ->set('shortname', 'My site')
      ->set('description', 'My site search')
      ->set('favicon-path', '/themes/contrib/mytheme/favicon.ico')
      ->save();
    $this->request = \Drupal::request();
  }

  /**
   * Test open search description.
   *
   * Test, that the the page exists and all XML tags are correctly displayed.
   */
  public function testOpenSearchDescription() {
    $this->drupalLogin($this->user);

    // Display the open search description page.
    $this->drupalGet('opensearchdescription');
    $this->assertSession()->statusCodeEquals(200);

    // Test the tags.
    $this->assertSession()->responseContains('<OpenSearchDescription xmlns="http://a9.com/-/spec/opensearch/1.1/" xmlns:moz="http://www.mozilla.org/2006/browser/search/">');
    $this->assertSession()->responseContains('</OpenSearchDescription>');
    $this->assertSession()->responseContains('<ShortName>My site</ShortName>');
    $this->assertSession()->responseContains('<Description>My site search</Description>');
    $this->assertSession()->responseContains('<Url type="text/html" method="get" template="' .
      $this->request->getSchemeAndHttpHost() . '/my-search?{searchTerms}"/>');
    $this->assertSession()->responseContains('<Url type="application/x-suggestions+json" method="get" template="' .
      $this->request->getSchemeAndHttpHost() . '/suggestions?{searchTerms}"/>');
    $this->assertSession()->responseContains('<Url type="application/x-suggestions+xml" method="get" template="' .
      $this->request->getSchemeAndHttpHost() . '/suggestions?{searchTerms}"/>');
    $this->assertSession()->responseContains('<moz:SearchForm>' .
      $this->request->getSchemeAndHttpHost() . '/my-search</moz:SearchForm>');
    $this->assertSession()->responseContains('<Image height="16" width="16" type="image/x-icon">' .
      $this->request->getSchemeAndHttpHost() . '/themes/contrib/mytheme/favicon.ico</Image>');
  }

}
