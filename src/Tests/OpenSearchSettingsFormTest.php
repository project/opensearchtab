<?php

namespace Drupal\opensearchtab\Tests;

use Drupal\Tests\BrowserTestBase;
use Drupal\user\Entity\Role;

/**
 * Tests the settings form.
 *
 * @group search
 * @group opensearchtab
 */
class OpenSearchSettingsFormTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['opensearchtab'];

  /**
   * Default theme.
   *
   * See: https://www.drupal.org/node/3083055
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * The role anonymous user.
   *
   * @var \Drupal\user\Entity\Role
   */
  private $guestRole;

  /**
   * Permissions to grant admin user.
   *
   * @var array
   */
  private $adminPermissions;

  /**
   * Permissions to grant guest user.
   *
   * @var array
   */
  private $guestPermissions;

  /**
   * An user with administration permissions.
   *
   * @var \Drupal\user\UserInterface
   */
  private $adminUser;

  /**
   * An user with guest permissions.
   *
   * @var \Drupal\user\UserInterface
   */
  private $guestUser;

  /**
   * Perform any initial set up tasks that run before every test method.
   *
   * Info to administrator permissions:
   * http://drupal.stackexchange.com/q/233416/72107
   */
  public function setUp(): void {
    parent::setUp();

    $this->guestRole = Role::load('anonymous');
    $this->guestPermissions = $this->guestRole->getPermissions();
    $this->guestUser = $this->drupalCreateUser($this->guestPermissions);

    $this->adminPermissions = array_keys(\Drupal::service(
      'user.permissions')->getPermissions()
    );
    $this->adminUser = $this->drupalCreateUser($this->adminPermissions);
  }

  /**
   * Test the settings form.
   *
   * Test, that the 'admin/config/search/opensearchtab' path returns
   * the right content and can be saved.
   */
  public function testSettingsFormAsAdmin() {

    $this->drupalLogin($this->adminUser);

    // Test the empty form.
    $this->drupalGet('admin/config/search/opensearchtab');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('See the XML of the open search tab:');

    $this->assertSession()->fieldValueEquals('opensearchtab_search_form_path', '');
    $this->assertSession()->fieldValueEquals('opensearchtab_search_path', '');
    $this->assertSession()->fieldValueEquals('opensearchtab_suggestions_path_json', '');
    $this->assertSession()->fieldValueEquals('opensearchtab_suggestions_path_xml', '');
    $this->assertSession()->fieldValueEquals('opensearchtab_shortname', '');
    $this->assertSession()->fieldValueEquals('opensearchtab_description', '');
    $this->assertSession()->fieldValueEquals('opensearchtab_favicon_path', '');

    $this->assertSession()->fieldNotExists('test_field');

    // Test saving the form.
    $edit = [
      'opensearchtab_search_form_path' => '/my-search',
      'opensearchtab_search_path' => '/my-search?{searchTerms}',
      'opensearchtab_suggestions_path_json' => '/suggestions?{searchTerms}',
      'opensearchtab_suggestions_path_xml' => '/suggestions?{searchTerms}',
      'opensearchtab_shortname' => 'My site',
      'opensearchtab_description' => 'My site search',
      'opensearchtab_favicon_path' => '/themes/contrib/mytheme/favicon.ico',
    ];
    $this->submitForm($edit, t('Save configuration'));

    $this->assertSession()->pageTextContains('The configuration options have been saved.');

    // Test the updated the form.
    $this->assertSession()->fieldValueEquals('opensearchtab_search_form_path', '/my-search');
    $this->assertSession()->fieldValueEquals('opensearchtab_search_path', '/my-search?{searchTerms}');
    $this->assertSession()->fieldValueEquals('opensearchtab_suggestions_path_json', '/suggestions?{searchTerms}');
    $this->assertSession()->fieldValueEquals('opensearchtab_suggestions_path_xml', '/suggestions?{searchTerms}');
    $this->assertSession()->fieldValueEquals('opensearchtab_shortname', 'My site');
    $this->assertSession()->fieldValueEquals('opensearchtab_description', 'My site search');
    $this->assertSession()->fieldValueEquals('opensearchtab_favicon_path', '/themes/contrib/mytheme/favicon.ico');

    $this->assertSession()->fieldNotExists('test_field');
  }

  /**
   * Test settings as guest.
   *
   * Tests that the 'admin/config/search/opensearchtab' path is not
   * accessible for guests.
   */
  public function testSettingsFormAsGuest() {
    $this->drupalLogin($this->guestUser);

    $this->drupalGet('admin/config/search/opensearchtab');
    $this->assertSession()->statusCodeEquals(403);
  }

}
