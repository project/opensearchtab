<?php

namespace Drupal\opensearchtab\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\opensearchtab\Services\OpenSearchDescriptionXmlService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Returns the XML content for the Open Search Description page.
 */
class OpenSearchDescriptionController extends ControllerBase {

  /**
   * A response object.
   *
   * @var \Symfony\Component\HttpFoundation\Response
   */
  protected $response;

  /**
   * Open Search Description XML.
   *
   * @var \Drupal\opensearchtab\Services\OpenSearchDescriptionXmlService
   */
  protected $openSearchDefinitionXml;

  /**
   * Class constructor.
   *
   * @param \Drupal\opensearchtab\Services\OpenSearchDescriptionXmlService $open_search_definition_xml
   *   Open Search Description XML.
   */
  public function __construct(OpenSearchDescriptionXmlService $open_search_definition_xml) {
    $this->response = new Response();
    $this->openSearchDefinitionXml = $open_search_definition_xml;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('opensearchtab.xml')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function content() {
    // Prepare the response.
    $this->response->setContent($this->openSearchDefinitionXml->getXml());
    $this->response->headers->set('Content-type', 'text/xml');

    return $this->response;
  }

}
