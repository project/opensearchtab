CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

This minimalistic Drupal module activates the 'Tab to Search' feature used in
Chrome by adding Open Search Description to the Drupal site. This enables the
visitors to search the website directly from the Google Chrome address bar.

The 'Tab to Search' feature is described in the article [Google Chrome – Tab to
Search](http://www.danielfuterman.com/google-chrome-tab-to-search/). How can a
website enable the 'Tab to Search' is described on the website of the Chromium
project: [Tab to Search](http://www.chromium.org/tab-to-search).

You can test the module on the site erneuerbareenergien.de (suggestions does
[not work](https://www.drupal.org/project/opensearchtab/issues/3105053) at the
time):

 1. Visit the [website](https://www.erneuerbareenergien.de) with Chrome.
 2. After that you should find a new search engine 'erneuerbareenergien.de' in
    'Chrome search settings / Other search engines'
    (chrome://settings/searchEngines).
 3. Open a new tab.
 4. Write 'erneuerbareenergien.de' in your Chrome address bar and click the
    'tab' key.
 5. You can take a look at the [Open Search
    Description](https://www.erneuerbareenergien.de/opensearchdescription) on
    the site.

More info:

 * For a full description of the module, visit
   [the project page](https://www.drupal.org/project/opensearchtab).

 * To submit bug reports and feature suggestions, or to track changes, visit
   [the issue page](https://www.drupal.org/project/issues/opensearchtab).

 * OpenSearch specification: [Github](https://github.com/dewitt/opensearch),
   [Mozilla.org](https://developer.mozilla.org/en-US/docs/Web/OpenSearch)

REQUIREMENTS
------------

There are no required modules.

RECOMMENDED MODULES
-------------------

There are no recommended modules.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See the
   [docs](https://www.drupal.org/documentation/install/modules-themes/modules-8)
   for further information.

CONFIGURATION
-------------

 * After installation the module will use your site's name as 'Short name',
   your site slogan as 'Description' and standard Drupal paths for search.

 * You can change the settings in » Configuration » Search and metadata »
   Open search tab.

TROUBLESHOOTING
---------------

There are not any known issues. See:

 * [Issues](https://www.drupal.org/project/issues/opensearchtab)
 * [Automated testing](https://www.drupal.org/node/2847577/qa)
 * [PAReview](
   https://pareview.sh/pareview/https-git.drupal.org-project-opensearchtab.git)

FAQ
---

There are no answers or issues.

MAINTAINERS
-----------

Current maintainers:

 * [Antonín Slejška](https://www.drupal.org/u/anton%C3%ADn-slej%C5%A1ka)
